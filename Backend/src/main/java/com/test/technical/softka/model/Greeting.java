/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.technical.softka.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Sebastian
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Greeting {

    private String name;
    private String typeGreeting;
    private String language;
}
