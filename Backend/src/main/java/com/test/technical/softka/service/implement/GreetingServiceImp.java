/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.technical.softka.service.implement;

import com.test.technical.softka.model.Greeting;
import com.test.technical.softka.service.GreetingService;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sebastian
 */
@Service
public class GreetingServiceImp implements GreetingService {

    @Override
    public String sendGreeting(Greeting greeting) {
        String newGreeting = "";
        if (greeting.getLanguage().equals("English")) {
            newGreeting = greetingEnglish(greeting);
        } else if (greeting.getLanguage().equals("Spanish")) {
            newGreeting = greetingSpanish(greeting);
        }
        return newGreeting;
    }

    public String greetingEnglish(Greeting greeting) {
        String greetingEn = "";
        switch (greeting.getTypeGreeting()) {
            case "Hi":
                greetingEn = "Hi " + greeting.getName() + ", How are you?";
                break;
            case "Name":
                greetingEn = "My name is " + greeting.getName();
                break;
            case "Bye":
                greetingEn = "Good bye " + greeting.getName() + ", See you soon";
                break;
            default:
                break;
        }
        return greetingEn;
    }

    public String greetingSpanish(Greeting greeting) {
        String greetingSp = "";
        switch (greeting.getTypeGreeting()) {
            case "Hi":
                greetingSp = "Hola " + greeting.getName() + ", ¿Como estas?";
                break;
            case "Name":
                greetingSp = "Mi nombre es " + greeting.getName();
                break;
            case "Bye":
                greetingSp = "Hasta luego " + greeting.getName() + ", nos vemos"
                        + " pronto";
                break;
            default:
                break;
        }
        return greetingSp;
    }
}
