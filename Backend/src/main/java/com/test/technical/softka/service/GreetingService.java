/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.technical.softka.service;

import com.test.technical.softka.model.Greeting;

/**
 *
 * @author Sebastian
 */
public interface GreetingService {

    String sendGreeting(Greeting greeting);
}
